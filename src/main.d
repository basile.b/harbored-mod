/**
 * D Documentation Generator
 * Copyright: © 2014 Economic Modeling Specialists, Intl.
 * Authors: Brian Schott
 * License: $(LINK2 http://www.boost.org/LICENSE_1_0.txt Boost License 1.0)
 */
module main;

import std.algorithm.iteration;
import std.array;
import std.conv;
import dparse.ast;
import dparse.lexer;
import dparse.parser;
import dparse.rollback_allocator;
import std.file;
import std.path;
import std.stdio;
import ddoc.lexer;

import allocator;
import config;
import macros;
import symboldatabase;
import tocbuilder;
import unittest_preprocessor;
import visitor;
import writer;

int main(string[] args)
{
	import std.datetime : Clock;
	const startTime = Clock.currStdTime;
	scope(exit) 
	{
		writefln("Time spent: %.3fs", (Clock.currStdTime - startTime) / 10_000_000.0); 
		// DO NOT CHANGE. hmod-dub reads this.
		writefln("Peak memory usage (kiB): %s", peakMemoryUsageK()); 
	}

	Config config;
	enum defaultConfigPath = "hmod.cfg";
	config.loadConfigFile(defaultConfigPath);
	config.loadCLI(args);
	
	if (config.doHelp)
	{
		import("help").writeln();
		return 0;
	}

	// Used to write default CSS/config with overwrite checking
	int writeProtected(string path, string content, string type)
	{
		if (path.exists)
		{
			writefln("'%s' exists. Overwrite? (y/N)", path);
			import std.ascii: toLower;
			char overwrite;
			readf("%s", &overwrite);
			if (overwrite.toLower != 'y')
			{
				writefln("Exited without overwriting '%s'", path);
				return 1;
			}
			writefln("Overwriting '%s'", path);
		}
		try
		{
			std.file.write(path, content);
		}
		catch (Exception e)
		{
			writefln("Failed to write default %s to file `%s` : %s",
				type, path, e.msg);
			return 1;
		}
		return 0;
	}

	if (config.doGenerateCSSPath.length)
	{
		writefln("Generating CSS file '%s'", config.doGenerateCSSPath);
		return writeProtected(config.doGenerateCSSPath, stylecss, "CSS");
	}
	if (config.doGenerateConfig)
	{
		writefln("Generating config file '%s'", defaultConfigPath);
		return writeProtected(defaultConfigPath, import("hmod.cfg"), "config");
	}

	try
	{
		config.macros = readMacros(config.macroFileNames);
	}
	catch (Exception e)
	{
		stderr.writeln(e.msg);
		return 1;
	}

	switch (config.format)
	{
		case "html-simple": generateDocumentation!HTMLWriterSimple(config); break;
		case "html-aggregated": generateDocumentation!HTMLWriterAggregated(config); break;
		default: writeln("Unknown format: ", config.format);
	}

	return 0;
}

private string[string] readMacros(const string[] macroFiles)
{
	import ddoc.macros : defaultMacros = DEFAULT_MACROS;

	string[string] result;
	defaultMacros.byKeyValue.each!(a => result[a.key] = a.value);
	result["D"]    = `<code class="d_inline_code">$0</code>`;
	result["HTTP"] = "<a href=\"http://$1\">$+</a>";
	result["WEB"]  = "$(HTTP $1,$2)";
	uniformCodeStyle(result);
	macroFiles.each!(mf => mf.readMacroFile(result));
	return result;
}

private void uniformCodeStyle(ref string[string] macros)
{
	macros[`D_CODE`] = `<pre><code class="hljs_d">$0</code></pre>`;
	macros[`D`] = `<b>$0</b>`;
	macros[`D_INLINECODE`] = `<b>$0</b>`;
	macros[`D_COMMENT`] = `$0`;
	macros[`D_KEYWORD`] = `$0`;
	macros[`D_PARAM`] = macros[`D_INLINECODE`];
}

private void generateDocumentation(Writer)(ref Config config)
{
	const string[] files = getFilesToProcess(config);
	import std.stdio : writeln;
	stdout.writeln("Writing documentation to ", config.outputDirectory);

	mkdirRecurse(config.outputDirectory);

	File search = File(buildPath(config.outputDirectory, "search.js"), "w");
	search.writeln(`"use strict";`);
	search.writeln(`var items = [`);

	auto database = gatherData(config, new Writer(config, search, null, null), files);

	TocItem[] tocItems = buildTree(database.moduleNames, database.moduleNameToLink);

	enum noFile = "missing file";
	string[] tocAdditionals = config
		.tocAdditionalFileNames
		.map!(path => path.exists ? readText(path) : noFile)
		.array ~ config.tocAdditionalStrings;
	if (!tocAdditionals.empty)
		foreach (ref text; tocAdditionals)
	{
		auto html = new Writer(config, search, null, null);
		auto writer = appender!string();
		html.readAndWriteComment(writer, text);
		text = writer.data;
	}

	foreach (f; database.moduleFiles)
	{
		writeln("Generating documentation for ", f);
		try
			writeDocumentation!Writer(config, database, f, search, tocItems, tocAdditionals);
		catch (DdocParseException e)
			stderr.writeln("Could not generate documentation for ", f, ": ", e.msg, ": ", e.snippet);
		catch (Exception e)
			stderr.writeln("Could not generate documentation for ", f, ": ", e.msg);
	}
	search.writeln(`];`);
	search.writeln(searchjs);

	// Write index.html and style.css
	writeln("Generating main page");
	std.file.write(buildPath(config.outputDirectory, "style.css"), getCSS(config.cssFileName));
	std.file.write(buildPath(config.outputDirectory, "highlight.pack.js"), hljs);
	std.file.write(buildPath(config.outputDirectory, "show_hide.js"), showhidejs);
	std.file.write(buildPath(config.outputDirectory, "anchor.js"), anchorjs);

	File index = File(buildPath(config.outputDirectory, "index.html"), "w");

	auto fileWriter = index.lockingTextWriter;
	auto html = new Writer(config, search, tocItems, tocAdditionals);
	html.writeHeader(fileWriter, "Index", 0);
	const projectStr = config.projectName ~ " " ~ config.projectVersion;
	const heading = projectStr == " " ? "Main Page" : (projectStr ~ ": Main Page");
	html.writeBreadcrumbs(fileWriter, heading);
	html.writeTOC(fileWriter);

	// Index content added by the user.
	if (config.indexFileName.length)
	{
		File indexFile = File(config.indexFileName);
		ubyte[] indexBytes = new ubyte[cast(uint) indexFile.size];
		indexFile.rawRead(indexBytes);
		html.readAndWriteComment(fileWriter, cast(string)indexBytes);
	}

	// A full list of all modules.
	if (database.moduleNames.length <= config.maxModuleListLength)
	{
		html.writeModuleList(fileWriter, database);
	}

	index.writeln(HTML_END);
}

/** Get the CSS content to write into style.css.
 *
 * If customCSS is not null, try to load from that file.
 */
string getCSS(string customCSS)
{
	if (customCSS.length == 0)
		return stylecss;
	try
		return readText(customCSS);
	catch(FileException e)
		stderr.writefln("Failed to load custom CSS `%s`: %s", customCSS, e.msg);
	return stylecss;
}

/// Creates documentation for the module at the given path
void writeDocumentation(Writer)(ref Config config, SymbolDatabase database, 
	string path, File search, TocItem[] tocItems, string[] tocAdditionals)
{
	LexerConfig lexConfig;
	lexConfig.fileName = path;
	lexConfig.stringBehavior = StringBehavior.source;

	// Load the module file.
	ubyte[] fileBytes;
	try
		fileBytes = cast(ubyte[]) path.readText!(char[]);
	catch (FileException e)
	{
		writefln("Failed to load file %s: will be ignored", path);
		return;
	}
	StringCache cache = StringCache(optimalBucketCount(fileBytes.length));
	RollbackAllocator allocator;
	Module m = fileBytes
		.getTokensForParser(lexConfig, &cache)
		.parseModule(path, &allocator, &ignoreParserError);
	
	auto htmlWriter  = new Writer(config, search, tocItems, tocAdditionals);
	auto visitor = new DocVisitor!Writer(config, database, getUnittestMap(m),
										 fileBytes, htmlWriter);
	visitor.visit(m);
}

/** Get .d/.di files to process.
 *
 * Files that don't exist, are bigger than config.maxFileSizeK or could not be
 * opened will be ignored.
 *
 * Params:
 *
 * config = Access to config to get source file and directory paths get max file size.
 * 
 * Returns: Paths of files to process.
 */
private string[] getFilesToProcess(ref const Config config)
{
	auto paths = config.sourcePaths.dup;
	auto files = appender!(string[])();
	void addFile(string path)
	{
		const size = path.getSize();
		if (size > config.maxFileSizeK * 1024)
		{
			writefln("WARNING: '%s' (%skiB) bigger than max file size (%skiB), " ~
					 "ignoring", path, size / 1024, config.maxFileSizeK);
			return;
		}
		files.put(path);
	}

	foreach (arg; paths)
	{
		if (!arg.exists)
			stderr.writefln("WARNING: '%s' does not exist, ignoring", arg);
		else if (arg.isDir) foreach (string fileName; arg.dirEntries("*.{d,di}", SpanMode.depth))
			addFile(fileName.expandTilde);
		else if (arg.isFile)
			addFile(arg.expandTilde);
		else
			stderr.writefln("WARNING: Could not open '%s', ignoring", arg);
	}
	return files.data;
}

/// Sink used to ignore error happening when parsing a module.
void ignoreParserError(string, size_t, size_t, string, bool) {}

/// String representing the script used to highlight.
immutable string hljs = import("highlight.pack.js");

/// String representing the default CSS.
immutable string stylecss = import("style.css");

/// String representing the script used to search.
immutable string searchjs = import("search.js");

/// String representing the script used to show or hide the navbar.
immutable string showhidejs = import("show_hide.js");

/// String representing the script used to put anchors on headers.
immutable string anchorjs = import("anchor.js");

private ulong peakMemoryUsageK()
{
	version(linux)
	{
		try
		{
			import std.exception : enforce;
			import std.algorithm.searching : startsWith;
			auto line = File("/proc/self/status").byLine().filter!(l => l.startsWith("VmHWM"));
			enforce(!line.empty, new Exception("No VmHWM in /proc/self/status"));
			return line.front.split()[1].to!ulong;
		}
		catch(Exception e)
		{
			writeln("Failed to get peak memory usage: ", e);
			return 0;
		}
	}
	else
	{
		writeln("peakMemoryUsageK not implemented on non-Linux platforms");
		return 0;
	}
}
