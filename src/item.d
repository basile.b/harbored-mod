/**
 * D Documentation Generator
 * Copyright: © 2014 Economic Modeling Specialists, Intl., © 2015 Ferdinand Majerech
 * Authors: Brian Schott, Ferdinand Majerech
 * License: $(LINK2 http://www.boost.org/LICENSE_1_0.txt Boost License 1.0)
 */
module item;

import formatter;
import std.algorithm;
import std.array: empty, array;
import dparse.ast;

/// Information associated to a particular declaration.
struct Item
{
	/** */ const string url;
	/** */ const string name;
	/** */ const string summary;
	/** */ const string type;

	/**
	 * AST node associated to the item.
	 *
	 * Used in a case per case basis to format extra info that
	 * cant be put directly as string.
	 */
	const ASTNode node;
}

/// Declarations of a module.
struct Members
{
	/** */ Item[] aliases;
	/** */ Item[] classes;
	/** */ Item[] enums;
	/** */ Item[] functions;
	/** */ Item[] interfaces;
	/** */ Item[] structs;
	/** */ Item[] templates;
	/** */ Item[] values;
	/** */ Item[] variables;

	/** */ Item[] publicImports;

	/// Write the list of public imports declared in a module.
	void writePublicImports(R, Writer)(ref R dst, Writer writer)
	{
		if (publicImports.empty)
			return;
		writer.writeSection(dst, 
		{
			writer.writeList(dst, "Public imports",
			{
				foreach (imp; publicImports) writer.writeListItem(dst,
				{
					if (imp.url.length)
					{
						dst.put(imp.name);
					}
					else writer.writeLink(dst, imp.url,
					{
						dst.put(imp.name);
					});
				});
			});
		}, "imports");
	}

	/// Write the table of members for a class/struct/module/etc.
	void write(R, Writer)(ref R dst, Writer writer)
	{
		if ([aliases, classes, enums, functions, interfaces, structs, templates, values, variables].all!empty)
			return;
		writer.writeSection(dst,
		{
			if (enums.length)       writer.writeItems(dst, enums, "Enums");
			if (aliases.length)     writer.writeItems(dst, aliases, "Aliases");
			if (variables.length)   writer.writeItems(dst, variables, "Variables");
			if (functions.length)   writer.writeItems(dst, functions, "Functions");
			if (structs.length)     writer.writeItems(dst, structs, "Structs");
			if (interfaces.length)  writer.writeItems(dst, interfaces, "Interfaces");
			if (classes.length)     writer.writeItems(dst, classes, "Classes");
			if (templates.length)   writer.writeItems(dst, templates, "Templates");
			if (values.length)      writer.writeItems(dst, values, "Values");
		}, "members");
	}
}
